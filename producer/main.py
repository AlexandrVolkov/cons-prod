from time import sleep
import os
from json import dumps
from kafka import KafkaProducer
from flask import Flask, render_template, escape, request, flash, redirect, url_for
from werkzeug.utils import secure_filename

producer = KafkaProducer(bootstrap_servers=['localhost'],
                         value_serializer=lambda x:
                         dumps(x).encode('utf-8'))

app = Flask(__name__)
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = '../files'

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def upload_form():
    return render_template('index.html')


@app.route('/', methods=['post'])
def send():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No file selected for uploading')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            flash('File successfully uploaded')
            return redirect('/')
        else:
            flash('Allowed file types are txt, pdf, png, jpg, jpeg, gif')
            return redirect(request.url)

    context = {
        'text': escape(request.form.get('text', '')),
    }

    try:
        future = producer.send('send_email', value={
            'text': context['text'],
        })
        record_metadata = future.get(timeout=10)

        print('--> The message has been sent to a topic: {}, partition: {}, offset: {}' \
              .format(record_metadata.topic,
                      record_metadata.partition,
                      record_metadata.offset))

    except Exception as e:
        print('--> It seems an Error occurred: {}'.format(e))

    return render_template('thanks.html', **context)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
