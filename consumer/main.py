from kafka import KafkaConsumer


consumer = KafkaConsumer(
    'send_email',
    bootstrap_servers=['172.17.0.1'],
    # auto_offset_reset='earliest',
    # enable_auto_commit=True,
    # group_id='my-group',
    # value_deserializer=lambda x: loads(x.decode('utf-8'))
)

for message in consumer:
    message = message.value
    print(f'{message.} recieved')
    # send(message['text'])
    # print(f'{message} added')
